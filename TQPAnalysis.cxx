#include "TQPOffline/TQPAnalysis.h"
#include "TopOfflineConfiguration/GlobalConfiguration.h"
#include "TQPOffline/chi2.h"
#include <math.h>
#include "TMinuit.h"
#include <map>
// #include "macros/BDTG.h"
// #include "RooUnfold/RooUnfold.h"

namespace TopOffline{

  TQPAnalysis::TQPAnalysis(std::string name) : ToolBase(true){
    m_name = name;
    auto* config = TopOffline::GlobalConfiguration::get();
    m_btaggingWP = (*config)("BTaggingWP");
    
    std::string muIDstring = (*config)("TQPAnalysis.SMTmuID"); // tight or medium+MI
    m_SMTmuID = -1;
    if(muIDstring=="medium+MI") m_SMTmuID = 0;
    if(muIDstring=="tight")     m_SMTmuID = 1;

    std::string btagSELstring = (*config)("TQPAnalysis.BTagSel"); // btagging selection (none, double-tag, one b outside, one b anywhere)
    m_BTagSel = 0; // none
    if(btagSELstring=="doubleTag")      m_BTagSel = 1; // double-tag
    if(btagSELstring=="oneTagOutside")  m_BTagSel = 2; // one b outside SMT jet
    if(btagSELstring=="oneTagAnywhere") m_BTagSel = 3; // one b anywhere
  }
    
	int smt_originIsC = 0;
	int smt_originIsB = 0;
	int smt_originIsLight = 0;
	int smt_originIsPrompt = 0;
	int smt_jetIsC = 0;
	int smt_jetIsB = 0;
	int smt_jetIsLight = 0;
	int chargeCorrPlusPlus   = 0;
	int chargeCorrMinusPlus  = 0;
	int chargeCorrPlusMinus  = 0;
	int chargeCorrMinusMinus = 0;
    bool isMVAapplied = true;
    
  //___________________________________________
  //
 
  void TQPAnalysis::apply(TopOffline::Event* event){
	bool isData = event->m_info->isData;
    bool chargeCorr =false;
	TLorentzVector softmu (0,0,0,0); 
	TLorentzVector checkLep_first  (0,0,0,0);        
	TLorentzVector checkLep_second (0,0,0,0); 
	TLorentzVector Zboson_check (0,0,0,0);
	TLorentzVector initial_jet(0,0,0,0);
	TLorentzVector initial_cjet(0,0,0,0);
	TLorentzVector jet (0,0,0,0); 
	TLorentzVector jet_isSMT_tlv(0,0,0,0);
    TLorentzVector b_jet_tlv (0,0,0,0); 
	TLorentzVector c_jet (0,0,0,0); 
	TLorentzVector Lead_lep(0,0,0,0);
    TLorentzVector Lead_lep_CR(0,0,0,0);
    TLorentzVector Lead_lep_VR(0,0,0,0);
	TLorentzVector MET(0,0,0,0);
	TLorentzVector b_jet_postChi (0,0,0,0); 
    TLorentzVector smt_jet_postChi (0,0,0,0);
	TLorentzVector neutrino_postChi(0,0,0,0);
    TLorentzVector ZLep1_tlv (0,0,0,0);
    TLorentzVector ZLep2_tlv (0,0,0,0);
    TLorentzVector WLep_tlv (0,0,0,0);
    TLorentzVector Zboson_tlv(0,0,0,0);
	TLorentzVector Wboson_tlv(0,0,0,0);
    TLorentzVector TopSM_tlv(0,0,0,0);
    TLorentzVector TopFCNC_tlv(0,0,0,0);
	std::vector<int> bjet_idx; bjet_idx.clear();
	
	float MUON_MASS = 105.6583745;	  
	float NeuLong = -999.e6;
	float mZ =  91.2e3;
    float Z_m = -999.e3;	
	int first_type  = -1;
	int second_type = -1;
	int i_lep=-1;
	int j_lep=-1;
	int k_lep=-1;
	int lep_idx_first  = -1;
	int lep_idx_second = -1;
	int lep_idx_third  = -1;
	int lep_charge_first  = -999;
	int lep_charge_second = -999;
	float lepPt = -999.e3;
	float lepPtMax = -999.e3;
	int lead_lep_Idx=-1;
	int N_lep = event->m_leptons.size();
	int N_jets = event->m_jets.size();
	int N_jets_sel = 0;
	int lep_Ptcut = 0;
	int N_bjets = 0;
	double chi2Min=999e3;
    double chi2 = -999e3;
	float mtw=-999e3;
    int softmu_q = 0.; 
    int Wlep_q = 0.; 
    int PRN = -999e3;
    double classifier = -999;
    Float_t chi2Min_post=-999e3;
    Float_t jet_isSMT_pt = -999e3, ZLep1_pt = -999e3, ZLep2_pt = -999e3, WLep_pt = -999e3, bjet_pt = -999e3, W_m = -999e3, W_pt = -999e3, TopFCNC_pt = -999e3, TopFCNC_m = -999e3, TopSM_m = -999e3, TopSM_pt = -999e3, Z_smtJet_eta, Z_bJet_eta, WLep_bJet_eta, WLep_smtJet_eta,  bJet_smtJet_eta, Z_WLep_eta, Z_smtJet_phi, Z_bJet_phi, WLep_bJet_phi, WLep_smtJet_phi, bJet_smtJet_phi, Z_WLep_phi ;    
    
    // MVA DECLARATION
    m_reader.resize(5);
    for(unsigned int iprn=0;iprn<5;iprn++){
        m_reader[iprn] = new TMVA::Reader( "!Color:!Silent" );
    }
    
	// get all the soft muons
	ParticleContainer smtmu = event->m_customObj["smtmu"];
    unsigned int Nsmtmu = smtmu.size();
	       
    // prepare new collections for selected soft muons
    ParticleContainer smtmu_tagged;          smtmu_tagged.clear();           // passing cuts (MI, tight, ...)
    TLorentzVector SMTmuon(0,0,0,0);
    TLorentzVector SMTmuon_ID(0,0,0,0);
    TLorentzVector SMTmuon_ME(0,0,0,0);
    
    for(unsigned int i_jet=0;i_jet<event->m_jets.size();i_jet++){
      event->m_jets[i_jet]->charVariable("jet_isSMTagged") = false;
    }
    
    int njetSMTagged = 0;
    // loop on soft muons
    for(unsigned int i_smtmu=0;i_smtmu<Nsmtmu;i_smtmu++){
      //
      // build 4-vectors
      std::shared_ptr<Particle> SMT = smtmu[i_smtmu];
      SMT->intVariable("smtmu_idx") = i_smtmu; // store the index of the original smtmu
      SMTmuon.SetPtEtaPhiE(   SMT->floatVariable("smtmu_pt"),
                              SMT->floatVariable("smtmu_eta"),
                              SMT->floatVariable("smtmu_phi"),
                              SMT->floatVariable("smtmu_E")    );
      SMTmuon_ID.SetPtEtaPhiE(SMT->floatVariable("smtmu_ID_pt"),
                              SMT->floatVariable("smtmu_ID_eta"),
                              SMT->floatVariable("smtmu_ID_phi"),
                              SMT->floatVariable("smtmu_ID_E") );
      SMTmuon_ME.SetPtEtaPhiE(SMT->floatVariable("smtmu_ME_pt"),
                              SMT->floatVariable("smtmu_ME_eta"),
                              SMT->floatVariable("smtmu_ME_phi"),
                              SMT->floatVariable("smtmu_ME_E") );
	  
      // Assign to SMT Particle it self the four vector
      SMT->p4(SMTmuon);

      // common cuts
      float smtmu_d0         = SMT->floatVariable("smtmu_d0");
      float smtmu_z0sintheta = SMT->floatVariable("smtmu_z0sintheta");
	 
      if(SMTmuon.Pt() < 4e3)                      continue;  // pT > 4 GeV
      if(fabs(smtmu_d0) > 3)                      continue;  // |d0| < 3 mm
      if(fabs(smtmu_z0sintheta) > 3)              continue;  // |z0*sinTheta| < 3 mm
 
      // calculate momentum imbalance
      float pID = fabs(SMTmuon_ID.P());
      float pME = fabs(SMTmuon_ME.P());
      float smtmu_momImbalance = ( pID - pME ) / pID;
      //
      // check if the muon is tight
      int quality = SMT->intVariable("smtmu_quality");
      int idcuts  = SMT->intVariable("smtmu_passIDcuts");
      bool isTight = (quality == 0) && (idcuts == 1);
      //
      // find the associated jet 
      float dRmin = 10.;
      int jetIdx = -1;

      for(unsigned int i_jet=0;i_jet<event->m_jets.size();i_jet++){
        if (event->m_jets[i_jet]->Pt()>25e3 && fabs(event->m_jets[i_jet]->Eta())<2.5){
            float dR = SMTmuon.DeltaR(event->m_jets[i_jet]->p4());
            if(dR<dRmin && dR<0.4){                                              // <--- UPDATED VALUE !!!
                jetIdx = i_jet;
                dRmin = dR;
            }
        }
      }
      // make sure to put here all the new decroations on smtmu, so that hthey are always filled before the continue is called
      SMT->intVariable("smtmu_jetIdx")    = jetIdx;
      SMT->floatVariable("smtmu_dRmin")   = dRmin;
      SMT->charVariable("smtmu_isTagged") = false;
      if(jetIdx<0) continue; // dR(µ,jet) < 0.4   
      
      // tight selection
      if(isTight && m_SMTmuID==1){
        SMT->charVariable("smtmu_isTagged") = true;
        smtmu_tagged.push_back( SMT );
      }
      
    }
    
    // END loop on soft muons
    // ------------------

    // loop on selected SMT muons
    // in case of more than one:
    // - pick the one with smallest dR in case jets are different FIXME: not used yet!
    // - pick the highest-pT one if both associated to the same jet
    float pTmax         = 0.;
    float pTmin         = 999.e3;
    float SMTmu_eta     = 0.;
    int SMTmu_charge    = 0 ;
    int SMTmu_origin    = 0 ;
    int SMTmu_jetIdx    = -999 ;
    bool SmtJetIsAntiBTag = false;
    //
    float weight_smtmuSF                           = 1.;
    float weight_smtmuSF_SMT_SF_ID_STAT_UP         = 1.;
    float weight_smtmuSF_SMT_SF_ID_STAT_DOWN       = 1.;
    float weight_smtmuSF_SMT_SF_ID_SYST_UP         = 1.;
    float weight_smtmuSF_SMT_SF_ID_SYST_DOWN       = 1.;
    float weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_UP   = 1.;
    float weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_DOWN = 1.;
    float weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_UP   = 1.;
    float weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_DOWN = 1.;    
    
    // this will be the SMT-muon with min pT in the event
    std::shared_ptr<Particle> SMT_min_pT = std::make_shared<Particle>();
    Nsmtmu = smtmu_tagged.size();
    
    // loop on selected SMT-muon
    for(unsigned int i_smtmu=0;i_smtmu<Nsmtmu;i_smtmu++){
      std::shared_ptr<Particle> SMT = smtmu_tagged[i_smtmu];
      if(SMT->Pt()<pTmin){
        SMT_min_pT = SMT;        
        pTmin = SMT->Pt();
      }
    }
    if(Nsmtmu == 0) SMTmu_jetIdx = -999;
    else SMTmu_jetIdx = SMT_min_pT->intVariable("smtmu_jetIdx");
    SMTmu_eta    = SMT_min_pT->Eta();	
    SMTmu_charge = SMT_min_pT->intVariable("smtmu_q");
    softmu       = SMT_min_pT->p4();
    
    // get origin
    SMTmu_origin = 0;  // 1:b, 2:c, 3:light(43,35), 4:prompt, 5: light other, 0:unknown
    if(!isData){
      int code = SMT_min_pT->intVariable("smtmu_truth_origin");
      if(     code==26) SMTmu_origin = 1; // b
      else if(code==29) SMTmu_origin = 1; // b
      else if(code==33) SMTmu_origin = 1; // b
      else if(code==25) SMTmu_origin = 2; // c
      else if(code==27) SMTmu_origin = 2; // c
      else if(code==32) SMTmu_origin = 2; // c
      else if(code==34) SMTmu_origin = 3; // light
      else if(code==35) SMTmu_origin = 3; // light
      else if(code==30) SMTmu_origin = 3; // light
      else if(code==31) SMTmu_origin = 3; // light
      else if(code==23) SMTmu_origin = 3; // light
      else if(code==24) SMTmu_origin = 3; // light
      else if(code < 0) SMTmu_origin = 3; // light
      else if(code== 9) SMTmu_origin = 4; // prompt (tau)
      else if(code==10) SMTmu_origin = 4; // prompt (t)
      else if(code==12) SMTmu_origin = 4; // prompt (W)
      else if(code==13) SMTmu_origin = 4; // prompt (Z)
      else if(code==14) SMTmu_origin = 4; // prompt (H)
    }
	
	MET.SetPtEtaPhiE(event->met_met,0,event->met_phi,event->met_met);	
	double MissPx = MET.Px();
	double MissPy = MET.Py();
	
	for(i_lep=0; i_lep<N_lep;i_lep++){
		if (event->m_leptons[i_lep]->Pt()>27e3 && N_lep==3)	lep_Ptcut++;
		lepPt = event->m_leptons[i_lep]->Pt();
		if(lepPt>lepPtMax){
			lepPtMax=lepPt;
			lead_lep_Idx=i_lep;
		}
	}
	
	bool OSSF= false;
	if (N_lep==3){
// First lepton
		for(i_lep=0; i_lep<N_lep-1;i_lep++){
			std::shared_ptr<Lepton> lep1 = event->m_leptons[i_lep];	
			checkLep_first = lep1->p4();
			lep_charge_first  = lep1->charge();
			TopOffline::leptonType lepTypeFirst = lep1->type();				
			if(lepTypeFirst==leptonType::electron)  first_type = 1;
			else if(lepTypeFirst==leptonType::muon) first_type = 2;
// Second lepton
			for(j_lep=i_lep+1; j_lep<N_lep;j_lep++){
				std::shared_ptr<Lepton> lep2 = event->m_leptons[j_lep]; 
				checkLep_second = lep2->p4();
				lep_charge_second  = lep2->charge();  
				TopOffline::leptonType lepTypeSecond = lep2->type(); 
				if(lepTypeSecond==leptonType::electron)  second_type = 1; 
				else if(lepTypeSecond==leptonType::muon) second_type = 2; 
				if(lep_charge_first*lep_charge_second == -1. && first_type == second_type ){
					OSSF = true;
					Zboson_check = checkLep_first+checkLep_second;
					if(fabs(Zboson_check.M()-mZ) < fabs(Z_m-mZ)){
						lep_idx_first  = i_lep;
						lep_idx_second = j_lep;
                        if     (checkLep_first.Pt() > checkLep_second.Pt()) {ZLep1_tlv = checkLep_first;  ZLep2_tlv = checkLep_second;}
                        else if(checkLep_first.Pt() < checkLep_second.Pt()) {ZLep1_tlv = checkLep_second; ZLep2_tlv = checkLep_first;}
						Z_m = Zboson_check.M();
					}
				}
			}
		}
		
// Third lepton 
		if(Z_m>0.){	// if the other two lep are foud take the third lep
			for (k_lep=0; k_lep<N_lep;k_lep++){
				std::shared_ptr<Lepton> lep3 = event->m_leptons[k_lep]; 
				if (k_lep!=lep_idx_first && k_lep!=lep_idx_second){
					lep_idx_third = k_lep;
                    WLep_tlv = lep3->p4();	// get the third lepton (W boson lep decay)		
				}				
			}
			mtw = sqrt(2. * WLep_tlv.Pt() * MET.Pt() * (1. - cos( WLep_tlv.DeltaPhi(MET)) ) );
			for(unsigned int i_jet=0;i_jet<N_jets;i_jet++){
				std::shared_ptr<Jet> Jet = event->m_jets[i_jet]; ;					
                if (Jet->Pt()>25e3 && fabs(Jet->Eta())<2.5){
					N_jets_sel++;
                    bjet_idx.push_back(i_jet);                    
                    if (Jet->charVariable("jet_isbtagged_MV2c10_77")) N_bjets++;
				}
			}
		}
	}

    if(lep_Ptcut>0 && Z_m>0.){
        
// ---------------------- CR Regions
//         if      (SMTmu_jetIdx<0) Lead_lep_CR = event->m_leptons[lead_lep_Idx]->p4();
        Lead_lep_CR = event->m_leptons[lead_lep_Idx]->p4();
// ---------------------- SR and VR Regions
        if (SMTmu_jetIdx>=0){ 
            //semileptonic correction
            float energy_loss=SMT_min_pT->floatVariable("smtmu_energy_loss");
            initial_cjet = event->m_jets[SMTmu_jetIdx]->p4();
            double energy_prime = initial_cjet.E() + 2*(softmu.E() - energy_loss); //approx: *2 because we add neutrino in the semileptinc correction
            double momentum_prime = std::sqrt(pow(energy_prime,2) - pow(MUON_MASS,2));
            double pz_prime = std::tanh(initial_cjet.Eta())*momentum_prime;
            double pt_prime = pz_prime/(std::sinh(initial_cjet.Eta()));		
            c_jet.SetPtEtaPhiE(pt_prime, initial_cjet.Eta(),initial_cjet.Phi(), energy_prime);
            // smt anti-BTagged 
            if (!event->m_jets[SMTmu_jetIdx]->charVariable("jet_isbtagged_MV2c10_77")) SmtJetIsAntiBTag = true; 
            
            // SMT charge correlation
            softmu_q = SMT_min_pT->intVariable("smtmu_q"); 
            Wlep_q = event->m_leptons[lep_idx_third]->charge(); 
            if      (softmu_q== 1 && Wlep_q== 1)  chargeCorrPlusPlus++;
            else if (softmu_q==-1 && Wlep_q== 1) {chargeCorrMinusPlus++; chargeCorr=true;}
            else if (softmu_q== 1 && Wlep_q==-1) {chargeCorrPlusMinus++; chargeCorr=true;}
            else if (softmu_q==-1 && Wlep_q==-1)  chargeCorrMinusMinus++;
            
            if (fabs(Z_m-91.2e3)<15e3 && mtw>40.e3 && N_jets_sel>=2 && chargeCorr) {
                Lead_lep_VR = event->m_leptons[lead_lep_Idx]->p4();
                for(unsigned int i_jet=0;i_jet<N_jets;i_jet++){
                    std::shared_ptr<Jet> Jet1 = event->m_jets[i_jet];
                    if (Jet1->Pt()>25e3 && fabs(Jet1->Eta())<2.5){     // 
                        if (bjet_idx[i_jet] == SMTmu_jetIdx) continue; // the b-jet (b-tagged or not) cannot be the smt jet
                        b_jet_tlv= event->m_jets[bjet_idx[i_jet]]->p4();	
                        TMinuit minuit(1);
                        minuit.SetPrintLevel(-1);
                        setConstParameters(ZLep1_tlv, ZLep2_tlv, WLep_tlv, b_jet_tlv, c_jet, MissPx, MissPy);
                        Double_t arglist[10];
                        Int_t ierflg = 0;	
                        minuit.SetFCN(fcn); // declare to Minuit the function to minimize
                        minuit.mnexcm("SET NOW", arglist, 1, ierflg); // no warnings
                        minuit.mnparm(0, "pZ_nu", 0., 1., -5000*GeV, 5000*GeV, ierflg); // define the minimizing variable
                        minuit.SetMaxIterations(5000);
                        minuit.Migrad();
                        double nuPz, nuPzerr;
                        minuit.GetParameter(0,nuPz,nuPzerr);
                        chi2 = getChi2(nuPz);
                        if(chi2 < chi2Min){
                            chi2Min = chi2;
                            neutrino_postChi.SetPxPyPzE(MissPx, MissPy, nuPz, sqrt(MissPx*MissPx + MissPy*MissPy + nuPz*nuPz));	
                            b_jet_postChi = b_jet_tlv;
                            smt_jet_postChi   = c_jet;
                        }
                    }
                }
                if(chi2Min>0 && fabs( (WLep_tlv + neutrino_postChi).M()-80.4e3 )<30e3 && fabs( (smt_jet_postChi + ZLep1_tlv + ZLep2_tlv).M()-172.5e3)<40e3 && fabs( (WLep_tlv + neutrino_postChi+b_jet_postChi).M()-172.5e3)<40e3 ){
                    Lead_lep = event->m_leptons[lead_lep_Idx]->p4();
                    jet_isSMT_tlv = smt_jet_postChi;
                    NeuLong = neutrino_postChi.Pz();
                    Zboson_tlv = ZLep1_tlv + ZLep2_tlv;
                    Wboson_tlv = WLep_tlv + neutrino_postChi;
                    TopSM_tlv = Wboson_tlv + b_jet_postChi;
                    TopFCNC_tlv = smt_jet_postChi + ZLep1_tlv + ZLep2_tlv;
                    chi2Min_post=chi2Min;
                    
                    // get c-tagged jet // truth d=1 u=2 s=3 c=4 b=5 t=6
                    if(!isData){	
                        // smt efficiency
                        if(SMTmu_origin==1) smt_originIsB++;  
                        else if(SMTmu_origin==2) smt_originIsC++;          	    // the taken c-jet (smt origin) is matched with truth info
                        else if(SMTmu_origin==3) smt_originIsLight++;
                        else smt_originIsPrompt++;    
                        
                        if( event->m_jets[SMTmu_jetIdx]->intVariable("jet_truthflav")==4 ) smt_jetIsC++;
                        else if( event->m_jets[SMTmu_jetIdx]->intVariable("jet_truthflav")==5 ) smt_jetIsB++;
                        else smt_jetIsLight++;
                    }
                    
                    srand(event->m_info->runNumber + event->m_info->mcChannelNumber + event->m_info->eventNumber);// generate a Psaudo-Random Number (PRN)
                    PRN = rand() % 5;
            
                    // Fill the MVA variable
                    jet_isSMT_pt= jet_isSMT_tlv.Pt();
                    ZLep1_pt= ZLep1_tlv.Pt(), 
                    ZLep2_pt= ZLep2_tlv.Pt(), 
                    WLep_pt= WLep_tlv.Pt(), 
                    bjet_pt= b_jet_tlv.Pt(), 
                    W_m= Wboson_tlv.M(), 
                    W_pt= Wboson_tlv.Pt(), 
                    TopFCNC_pt= TopFCNC_tlv.Pt(), 
                    TopFCNC_m= TopFCNC_tlv.M(), 
                    TopSM_pt= TopSM_tlv.Pt();
                    TopSM_m= TopSM_tlv.M();
                    
                    Z_smtJet_eta  = (Zboson_tlv-jet_isSMT_tlv).Eta();
                    Z_bJet_eta    = (Zboson_tlv-b_jet_tlv).Eta();
                    Z_WLep_eta    = (Zboson_tlv-WLep_tlv).Eta();
                    WLep_bJet_eta = (WLep_tlv-b_jet_tlv).Eta();
                    WLep_smtJet_eta = (WLep_tlv-jet_isSMT_tlv).Eta();
                    bJet_smtJet_eta = (b_jet_tlv-jet_isSMT_tlv).Eta();
                    
                    Z_smtJet_phi  = (Zboson_tlv-jet_isSMT_tlv).Phi();
                    Z_bJet_phi    = (Zboson_tlv-b_jet_tlv).Phi();
                    Z_WLep_phi    = (Zboson_tlv-WLep_tlv).Phi();
                    WLep_bJet_phi = (WLep_tlv-b_jet_tlv).Phi();
                    WLep_smtJet_phi = (WLep_tlv-jet_isSMT_tlv).Phi();
                    bJet_smtJet_phi = (b_jet_tlv-jet_isSMT_tlv).Phi();

                    //MVA APPLICATION
                    if (isMVAapplied && PRN>=0) {
                        // load the weight files for the readers
                        m_reader[PRN]->AddVariable( "jet_isSMT_pt",  &jet_isSMT_pt );
                        m_reader[PRN]->AddVariable( "mtw",           &mtw );
                        m_reader[PRN]->AddVariable( "chi2Min",       &chi2Min_post );
                        m_reader[PRN]->AddVariable( "ZLep1_pt",      &ZLep1_pt );
                        m_reader[PRN]->AddVariable( "ZLep2_pt",      &ZLep2_pt );
                        m_reader[PRN]->AddVariable( "WLep_pt",       &WLep_pt );
                        m_reader[PRN]->AddVariable( "bjet_pt",       &bjet_pt );
                        m_reader[PRN]->AddVariable( "W_m",           &W_m );
                        m_reader[PRN]->AddVariable( "W_pt",          &W_pt );
                    //  m_reader[PRN]->AddVariable( "TopFCNC_pt",    &TopFCNC_pt );
                        m_reader[PRN]->AddVariable( "TopFCNC_m",     &TopFCNC_m );
                    //  m_reader[PRN]->AddVariable( "TopSM_pt",      &TopSM_pt );
                        m_reader[PRN]->AddVariable( "TopSM_m",       &TopSM_m );
                        
                        m_reader[PRN]->AddVariable( "Z_smtJet_eta",  &Z_smtJet_eta );
                        m_reader[PRN]->AddVariable( "Z_bJet_eta",    &Z_bJet_eta );
                        m_reader[PRN]->AddVariable( "Z_WLep_eta",    &Z_WLep_eta );
                        m_reader[PRN]->AddVariable( "WLep_bJet_eta", &WLep_bJet_eta );
                        m_reader[PRN]->AddVariable( "WLep_smtJet_eta", &WLep_smtJet_eta ); //to remove
                        m_reader[PRN]->AddVariable( "bJet_smtJet_eta", &bJet_smtJet_eta ); //to remove
                      
                        m_reader[PRN]->AddVariable( "Z_smtJet_phi",  &Z_smtJet_phi );
                        m_reader[PRN]->AddVariable( "Z_bJet_phi",    &Z_bJet_phi );
                        m_reader[PRN]->AddVariable( "Z_WLep_phi",    &Z_WLep_phi ); 
                        m_reader[PRN]->AddVariable( "WLep_bJet_phi", &WLep_bJet_phi ); 
                        m_reader[PRN]->AddVariable( "WLep_smtJet_phi", &WLep_smtJet_phi ); //to remove
                        m_reader[PRN]->AddVariable( "bJet_smtJet_phi", &bJet_smtJet_phi ); //to remove
                        
                        m_reader[PRN]->BookMVA( "BDTG", Form("../../TopOffline/TQPOffline/macros/BDTG/weights/MVAnalysis%d_BDTG.weights.xml",PRN) );
                        classifier = m_reader[PRN]->EvaluateMVA("BDTG");               
                    }        
                }
            }
        }        
    }
    for (int iprn=0; iprn<5; iprn++) delete m_reader[iprn];

// SFs
    if(!isData){
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_SF"))                         weight_smtmuSF                           = SMT_min_pT->floatVariable("smtmu_recoeff_tight_SF");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_UP"))         weight_smtmuSF_SMT_SF_ID_STAT_UP         = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_UP");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_DOWN"))       weight_smtmuSF_SMT_SF_ID_STAT_DOWN       = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_DOWN");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_UP"))         weight_smtmuSF_SMT_SF_ID_SYST_UP         = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_UP");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_DOWN"))       weight_smtmuSF_SMT_SF_ID_SYST_DOWN       = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_DOWN");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_LOWPT_UP"))   weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_UP   = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_LOWPT_UP");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_LOWPT_DOWN")) weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_DOWN = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_STAT_LOWPT_DOWN");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_LOWPT_UP"))   weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_UP   = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_LOWPT_UP");
        if(SMT_min_pT->checkFloatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_LOWPT_DOWN")) weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_DOWN = SMT_min_pT->floatVariable("smtmu_recoeff_tight_syst_SF_ID_SYST_LOWPT_DOWN");
    }
//-----------------------------------------------------------	
//     std::cout<<" smt_originIsB     = "<<smt_originIsB<<std::endl;
//     std::cout<<" smt_originIsC     = "<<smt_originIsC<<std::endl;
//     std::cout<<" smt_originIsLight = "<<smt_originIsLight<<std::endl;
//     std::cout<<" smt_jetIsB        = "<<smt_jetIsB<<std::endl;
//     std::cout<<" smt_jetIsC        = "<<smt_jetIsC<<std::endl;
//     std::cout<<" smt_jetIsLight    = "<<smt_jetIsLight<<std::endl;

	float ttbar_XSec = 831.76;
    float utZ_XSec = 0.37;
    float ctZ_XSec = 0.068;
	float BrZll = (0.03363+0.03366+0.0337);
	float BrWlnu = (0.1071+0.1063+0.1138);  
    float weightXsec_ttbar = 2.*ttbar_XSec*BrZll*BrWlnu; //factor 2 because top or antitop could decay FCNC  
    float weightXsec_utZ   =      utZ_XSec*BrZll*BrWlnu; 
    float weightXsec_ctZ   =      ctZ_XSec*BrZll*BrWlnu;   
//----------	
  
    event->intVariable("SMTmu_jetIdx")   = SMTmu_jetIdx;
    event->intVariable("N_jets_sel")     = N_jets_sel;  // used for BDT
	event->intVariable("N_bjets")        = N_bjets;
	event->floatVariable("chi2Min")      = chi2Min_post;  // used for BDT
	event->floatVariable("Lead_lep")     = Lead_lep.Pt();
    event->floatVariable("Lead_lep_CR")  = Lead_lep_CR.Pt();
    event->floatVariable("Lead_lep_VR")  = Lead_lep_VR.Pt();
    event->floatVariable("ZLep1_pt")     = ZLep1_tlv.Pt();  // used for BDT
    event->floatVariable("ZLep2_pt")     = ZLep2_tlv.Pt();  // used for BDT
    event->floatVariable("WLep_pt")      = WLep_tlv.Pt();    // used for BDT
    event->floatVariable("jet_isSMT_pt") = jet_isSMT_tlv.Pt(); // used for BDT
    event->floatVariable("bjet_pt")      = b_jet_tlv.Pt();  // used for BDT
	event->floatVariable("NeuLong")      = NeuLong;
	event->floatVariable("MET") 	     = MET.Pt();
    event->floatVariable("mtw")          = mtw;   // used for BDT
    event->floatVariable("Z_m")          = Z_m;
    event->floatVariable("W_m")          = Wboson_tlv.M();  // used for BDT
    event->floatVariable("W_pt")         = Wboson_tlv.Pt();  // used for BDT
    event->floatVariable("TopSM_m")      = TopSM_tlv.M();    // used for BDT
//  event->floatVariable("TopSM_pt")     = TopSM_tlv.Pt(); // used for BDT
    event->floatVariable("TopFCNC_m")    = TopFCNC_tlv.M(); // used for BDT
//  event->floatVariable("TopFCNC_pt")   = TopFCNC_tlv.Pt(); // used for BDT  
    
    event->floatVariable("Z_smtJet_eta")  = Z_smtJet_eta; // used for BDT
    event->floatVariable("Z_bJet_eta")    = Z_bJet_eta; // used for BDT
    event->floatVariable("WLep_bJet_eta") = WLep_bJet_eta; // used for BDT
    event->floatVariable("Z_smtJet_phi")  = Z_smtJet_phi; // used for BDT
    event->floatVariable("Z_bJet_phi")    = Z_bJet_phi; // used for BDT
    event->floatVariable("WLep_bJet_phi") = WLep_bJet_phi; // used for BDT 
        
    event->floatVariable("Z_WLep_eta")      = Z_WLep_eta; // used for BDT
    event->floatVariable("WLep_smtJet_eta") = WLep_smtJet_eta; // used for BDT
    event->floatVariable("bJet_smtJet_eta") = bJet_smtJet_eta; // used for BDT
    event->floatVariable("Z_WLep_phi")      = Z_WLep_phi; // used for BDT
    event->floatVariable("WLep_smtJet_phi") = WLep_smtJet_phi; // used for BDT
    event->floatVariable("bJet_smtJet_phi") = bJet_smtJet_phi; // used for BDT 
       
	event->floatVariable("weightXsec_ttbar")   = weightXsec_ttbar; 
    event->floatVariable("weightXsec_utZ")     = weightXsec_utZ;
    event->floatVariable("weightXsec_ctZ")     = weightXsec_ctZ;
	event->intVariable("chargeCorrPlusPlus")   = chargeCorrPlusPlus; 
	event->intVariable("chargeCorrMinusPlus")  = chargeCorrMinusPlus; 
	event->intVariable("chargeCorrPlusMinus")  = chargeCorrPlusMinus; 
	event->intVariable("chargeCorrMinusMinus") = chargeCorrMinusMinus; 
    event->intVariable("chargeCorr")           = chargeCorr;
    event->intVariable("SmtJetIsAntiBTag")     = SmtJetIsAntiBTag;
    event->intVariable("PRN")                  = PRN; 
    event->floatVariable("classifier")         = classifier;
    
    event->floatVariable("weight_smtmuSF")                           = weight_smtmuSF;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_STAT_UP")         = weight_smtmuSF_SMT_SF_ID_STAT_UP;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_STAT_DOWN")       = weight_smtmuSF_SMT_SF_ID_STAT_DOWN;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_SYST_UP")         = weight_smtmuSF_SMT_SF_ID_SYST_UP;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_SYST_DOWN")       = weight_smtmuSF_SMT_SF_ID_SYST_DOWN;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_UP")   = weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_UP;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_DOWN") = weight_smtmuSF_SMT_SF_ID_STAT_LOWPT_DOWN;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_UP")   = weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_UP;
    event->floatVariable("weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_DOWN") = weight_smtmuSF_SMT_SF_ID_SYST_LOWPT_DOWN;
  }
}
