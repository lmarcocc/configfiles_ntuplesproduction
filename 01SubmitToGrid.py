#!/usr/bin/env python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import MC16_TOPQ1
import Data_rel21

config = TopExamples.grid.Config()
config.CMake         = True
config.code          = 'top-xaod'

config.gridUsername  = 'lmarcocc'
config.suffix        = '11Oct'
config.excludedSites = ''
config.noSubmit      = False
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-StTHGRID-BHAM-HEP_LOCALGROUPDISK'
maxNFilesPerJob = '10'
otherOptions    = ''
checkPRW        = True

def details(self):
    cutsFileIsARealFile = checkForFile(self.settingsFile)
    txt = '(' + logger.FAIL + 'not found' + logger.ENDC + ')'
    if cutsFileIsARealFile:
        txt = '(exists)'

    print logger.OKBLUE + 'Analysis Settings:' + logger.ENDC
    print ' -Code:          ', self.code
    print ' -CutsFile:      ', self.settingsFile, txt

    print logger.OKBLUE + 'Grid Settings:' + logger.ENDC
    print ' -GridUsername:  ', self.gridUsername
    print ' -Suffix:        ', self.suffix
    print ' -ExcludedSites: ', self.excludedSites
    print ' -ForceSite:     ', self.forceSite
    print ' -NoSubmit:      ', self.noSubmit
    print ' -MergeType:     ', self.mergeType, 'out of (None, Default, xAOD)'
    print ' -memory:        ', self.memory, 'in MB'
    print ' -maxNFilesPerJob', self.maxNFilesPerJob        
    print ' -otherOptions:  ', self.otherOptions 
    print ' -checkPRW:      ', self.checkPRW

    txt = self.destSE
    if len(txt) == 0:
        txt = '<not set>'
    print ' -DestSE         ', txt

    print ''

#################################################################################
####                           DATA
#################################################################################

config.settingsFile  = 'config_tqZ_mc16a.txt'

names = ['Data15_TOPQ1',
         'Data16_TOPQ1',
         'Data17_TOPQ1',
         'Data18_TOPQ1',
         ]

samples = TopExamples.grid.Samples(names)
TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)

#################################################################################
####                           MC16a
#################################################################################

names = ['TOPQ1_ttbar_signal_old_mc16a',
	     'TOPQ1_ttbar_PowPy8_mc16a',
	     'TOPQ1_ttbar_PowhegHerwig7_mc16a',
		 'TOPQ1_ttV_mc16a',
		 'TOPQ1_ttV_Sherpa_mc16a',
		 'TOPQ1_ttV_A14Var_mc16a',
		 'TOPQ1_tZ_mc16a',
		 'TOPQ1_tZ_syst_mc16a',
		 'TOPQ1_tWZ_mc16a',
		 'TOPQ1_tWZ_syst_mc16a',
		 'TOPQ1_diboson_sherpa222_mc16a',
		 'TOPQ1_diboson_PowhegPy8EG_mc16a',
		 'TOPQ1_Wt_mc16a',
		 'TOPQ1_Zjets_PP8_mc16a',	     
		 'TOPQ1_Zjets_Sherpa221_mc16a',
        ]

#names = ['TOPQ1_signal_mc16a'
        #]

samples = TopExamples.grid.Samples(names)
TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)

#################################################################################
####                           MC16d
#################################################################################

config.settingsFile  = 'config_tqZ_mc16d.txt'

names = ['TOPQ1_ttbar_signal_old_mc16d',		 
	     'TOPQ1_ttbar_PowPy8_mc16d',
	     'TOPQ1_ttbar_PowhegHerwig7_mc16d',
		 'TOPQ1_ttV_mc16d',
		 'TOPQ1_ttV_Sherpa_mc16d',
		 'TOPQ1_ttV_A14Var_mc16d',
		 'TOPQ1_tZ_mc16d',
		 'TOPQ1_tZ_syst_mc16d',
		 'TOPQ1_tWZ_mc16d',
		 'TOPQ1_tWZ_syst_mc16a',
		 'TOPQ1_diboson_sherpa222_mc16d',
		 'TOPQ1_diboson_PowhegPy8EG_mc16d',
		 'TOPQ1_Wt_mc16d',
		 'TOPQ1_Zjets_PP8_mc16d',	     
		 'TOPQ1_Zjets_Sherpa221_mc16d',
        ]

#names = ['TOPQ1_signal_mc16d'
        #]

samples = TopExamples.grid.Samples(names)
TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)

#################################################################################
####                            MC16e
#################################################################################

config.settingsFile  = 'config_tqZ_mc16e.txt'

names = ['TOPQ1_ttbar_PowPy8_mc16e',
	     'TOPQ1_ttbar_PowhegHerwig7_mc16e',
		 'TOPQ1_ttV_mc16e',
		 'TOPQ1_ttV_Sherpa_mc16e',
		 'TOPQ1_ttV_A14Var_mc16e',
		 'TOPQ1_tZ_mc16e',
		 'TOPQ1_tZ_syst_mc16e',
		 'TOPQ1_tWZ_mc16e',
		 'TOPQ1_tWZ_syst_mc16e',
		 'TOPQ1_diboson_sherpa222_mc16e',
		 'TOPQ1_diboson_PowhegPy8EG_mc16e',
		 'TOPQ1_Wt_mc16e',
		 'TOPQ1_Zjets_PP8_mc16e',	     
		 'TOPQ1_Zjets_Sherpa221_mc16e',
        ]

#names = ['TOPQ1_signal_mc16e'
        #]

samples = TopExamples.grid.Samples(names)
TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)
