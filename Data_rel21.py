# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration                                                                                                                            
import TopExamples.grid
import TopExamples.ami

# Period containers                                                                                                         
DATA15 = ['data15_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp15_v01_p3794',                                          
         ]

DATA16 = ['data16_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp16_v01_p3794',	   
          ]

DATA17 = ['data17_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp17_v01_p3794',                                                         
          ]

DATA18 = ['data18_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp18_v01_p3794',
         ]

# Period containers - preferred choice                                                                                      
TopExamples.grid.Add('Data15_TOPQ1').datasets = [i.format('TOPQ1') for i in DATA15]

TopExamples.grid.Add('Data16_TOPQ1').datasets = [i.format('TOPQ1') for i in DATA16]

TopExamples.grid.Add('Data17_TOPQ1').datasets = [i.format('TOPQ1') for i in DATA17]

TopExamples.grid.Add('Data18_TOPQ1').datasets = [i.format('TOPQ1') for i in DATA18]
